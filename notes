1. setup the project structure using python-poetry [DONE]

2. plan out the api: [DONE]
    - purpose/design:
        - the purpose of this API is going to be to get a list of sources the client can use to stream a given item
        - it will be an REST API

        - it will do this in the following steps:
            1. parser the users request, determine if it's a movie or tv-show
            2. search the providers and extract an i-frame containning a video-player for the given item (e.g. movie/tv-episode)
                - we can't seperate the process of searching providers and getting an i-frame because some providers keep all tv-shows in a single link and load episodes via different i-frames
                - hence we can't just return a link, but rather it's easier to just return the i-frame
            3. extract the media-sources from each i-frame
            4, respond to request with all media-sources

        - dealing with errors:
            - unknow domain-name when extracting from i-frames:
                - if encounter an unknown domain when attempting to extract sources from an i-frame we'll skip the domain and log an error message specifying we encountered an unknown source
                    - in future we could graph these errors to see which future domains we want to add to our extractors

            - no links for the given item:
                - will return an empty sources list

            - invalid request:
                - any request missing any detail or required info
                - the error will be returned to the user

            - unknown errors:
                - any server-side errors will be logged but returned to the client

    - requests and response format and design:
        - request
            - so based on quick google search it's a bad pracice to rely on the data in the body of a get request
            - meaning we'll either have to use paramaters with GET
            - or use POST with json
            - GET is simpler, however the JSON allows us to be consitent 
            - JSON also provides better room for expansion should we need to add more paramaters
            - we'll go with JSON:
                - easier to parse requests
                - keep requests and responses in the same format
                - allows us to easily add more detail in the future should we need to, e.g. request multiple episodes etc.
            
            - format:
                - since they need different data tv-shows and movies are going to have seperate request routes and formats
                - tv:
                    {
                        "name": (str) "full name of the tv-show",
                        "season": (int) SEASON_NUMBER,
                        "episode": (int) EPISODE_NUMBER
                        "release-year": (int) RELEASE_YEAR
                    }
                - move:
                    {
                        "name": "full name of the tv show"
                        "release-year": (int) RELEASE_YEAR
                    }

                NOTE: we're also including the release year so that we have a way to differ items with different names

        - response:
            - both tv and movie requests will have the same response format, that is:
                {
                    "sources": [
                        {"url": source-link, "steaming-type": 0 or 1},
                        ...
                    ],
                    "errors": []
                }

                NOTE: streaming-type paramater specfied if we can directly update the src attribute or if we need to use HLS to stream the source
                    - where 0 is direct and 1 is HLS

    - API routes
        - since tv shows and movies have different data we're going to give them different routes
        - thoose are:
            IP:PORT/sources/movie/
            IP:PORT/souces/tvshow/

    - HTTP REST framework
        - since we're doing a sync api and not using web-sockets we'll keep it simple and use falcon
        - it's fast, reliable and we're already familiar with it.

    - classes/functions:
        - to implement the above we'll need the following functionality:
            - getting i-frames for the given item, which can be broken into 2 steps:
                - searching providers for links
                - extracting i-frames from said links

                (these steps may vary slightly for tv-shows depending on the provider)

            - extracting sources from the i-frame
                - will need an extractor per method (or per domain-name)

            - http-server

        - classes:
            - http-server:
                - responsible for dealing with api requests and responses

            - provider-searcher:
                - responsible for searching providers for links to the given item

            - iframe extractors:
                - extract the iframe from the given items url
                - will be an extractor per domain

            - source extractors:
                - responsible for extracting sources from an i-frame
                - will have an extractor per domain or per-method (if more than one domain shares a method)

            - factories:
                - source extractors:
                    - will return the extractor for a given domain
                
                - searcher:
                    - will return the search method for a given provider
                    - will take input of (provider, movie_or_tv_show)

                - iframe extractors
                    - will return extractor for the given domain

    - testing
        - i know its just a personal project, however it'd still be a good idea to add in some tests
        - unit-tests
            - checking the indvidual components work
        - behave-tests:
            - checking api end-points

3. implement [DOING]
    - http-server [DONE]
        - flow of data on the server:
            1. recive the post request
            2. check that we have the expected data
            3. call the searcher
            4. based on the results of the 

        - document the API in the readme 


    - source extractors [DONE]
        - re-write the existing procs to use multiproccessing [DOING]
            - use the existing extractors
            - but update them so that they run using multi-processing

            - IDEA: (NO we can't...) can we change them to be singeltons so that we only have to load the browser process once??? [DONE]
                - we can
                - we then need to figure out how to run the extract process with multiproccessing

                - so the flow would be:
                    - create singleton objects within the factory
                    - return singleton
                    - execute extract as a process so that we can extract from multiple domains at once

                - so we basically want a class that can do the following:
                    - be init once as a singleton
                    - run a called method inside a process
                    - be able to check if the called method has finished or not

                - NOTE: we can't do this, since we have the possiblity of two iframes having the same source-provider they would both use the driver
                    - this would cause the webdriver to go from the current page to the new page messing up the results


            - extractors:
                - jwplayer [DONE]
                - movcloud [DONE]
                - streamtape [DONE]
                - twoembed 
                    - run test [DOING]
                - vidnext
                    - run test [DOING]

        - create factory [DONE]

        - create a wrapper/manager for the handler to pass multiple iframes into [DONE]

        - BUG: [DONE]
            - the firefox process is not being killed on exit

    - searcher [DOING]
        - figure out how we want to structure this... [DOING]

        - need 3 things:
            1. a way to generate the search url
            2. a way to extract an href from within the search page
            3. a adapater that allows us to search against all providers


        - so we'll have the following classes/methods:
            - class for generating urls with different methods

            - for every provider:
                - item link extractor [DONE]
                    - one for tv and one for movie

                    - do the following to start with: 
                        - vidnext [DONE]
                        - bobmovies [DONE]
                        - gomovies.beer [DONE]
                

                - searcher: [DONE]
                    - will make use of the url-generator and href-extractor
                    - again, there will be one for tv and one movies

                    - write a searcher for each provider OR one searcher that takes in the following params:
                        - movie link extractor
                        - tv link extractor
                        - url creator

                        then we just need one and can pass the different sub-classes for each provider

                    NOTE: we can use the searcher class as a facade to wrap up the url-creator, link extractor and iframe extractor

    - iframe extractor [DONE]

    
    - update the API to include season release-year and show-release year: [DONE]
        - then update any extractors that use the release-year

        NOTE: example use-case bobmovies uses show release-year whereas gomovies uses the season release-year

    - an executor class for running all the searchers [DONE]

5. QUESTION: would it be more effective to have a singleton webdriver??? - particulary for source extraction [DONE]
    - curious if it would be faster to extract sources sequentially using a single driver instead of simultaneously using multi-processes and drivers
    - in other words, does the overhead of having to spawn a webdriver elimante the advatange given to us by using multiproccessing???

    - same question applies for the searchers

    - where do we use the webdriver???
        - source extractors
        - iframe extractors

    - since the searchers are already single processed we'll use them to test our theory [DONE]
        - will be more work changing the source extractors to be single process

        - results:
            - after the inital request where we spawn the driver
            - baring any timeouts, the searcher is almost instant
            - compared to being ~5s for each when we spawn the driver

        - test it with source extraction [DOING]

    - update all extarctors to use the singleton web-driver [DONE]


    TODO: need to find a way to ensure the webdriver is killed when we close the program [DONE]
        - lets assume right now that since we're catching all errors in the handler that it fatal errors won't be an issue
        - instead lets focus on clean exits
        - use signals instead of atexit for future proofing, e.g. killing it through kuberntues


6. QUESTION: do we want to categorize the types of streams into STREAMING and DIRECT or do we want to leave it upto the end-user???


5. optimization ideas:
    IDEA: cache idea:
        - might be an idea to have some form of cache so that we don't have to keep requesting the same links over and over
        - would have to be sure to check the link has expired before retunring though
    
    IDEA: web-driver pool idea
        - allow the user to specify how many web-drivers to spawn
        - then during startup we spawn these into a pool
        - then when extracting sources we can pull from this pool to run multiple extractors at once without the downside of having to spawn in the web-drivers on the fly (which as we know is slow af)
            - if pool is empty we wait until its not to spawn the next extractor

4. test

5. put into k8s or docker so that we can easily deploy it