class Providers:
    ''' Enum of all providers used in searching and extracting
        sources. They are all in a single ENUM since some providers
        are used in both searching and extracting sources. '''
    VIDNEXT     = "https://vidnext.net"
    VIDCLOUD9   = "https://vidcloud9.com"
    VIDCLOUD    = "https://vidcloud.pro"
    TWOEMBED    = "https://www.2embed.ru"
    UPSTREAM    = "https://upstream.to"
    STREAMSB    = "https://streamsb.net"
    STREAMTAPE  = "https://streamtape.com"
    MOVCLOUD    = "https://movcloud.net"
    GOMOVIES    = "https://gomovies.beer"
    BOBMOVIES   = "https://bobmovies.cc"