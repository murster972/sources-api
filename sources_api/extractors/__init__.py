import requests
import time

from selenium import webdriver
from selenium.webdriver.firefox.options import Options

def abs_address(source, provider):
    ''' anesure we have an absolute adddress '''
    if source.startswith("/") and not source.startswith("//"):
        # we need 2 condts since relative will be "/"
        # but // is already valid so we don't want to change it
        source = f"{provider}{source}"

    return source 

def enforce_protocol(url):
    ''' enforce https on any urls that start with // '''
    if url.startswith("//"):
        url = url.replace("//", "https://")

    return url

def get_html(url, timeout=2):
    ''' load the given url and return the loaded html '''
    user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"
    r = requests.get(url, headers={"user-agent": user_agent}, timeout=timeout)

    return r.text


class WebDriver:
    ''' wrapper to ensure we use the web-driver as a singleton '''
    
    _DRIVER = None

    def __init__(self, firefox_driver):
        if not self._driver:
            options = Options()
            options.headless = True

            print("creating driver")

            self._driver = webdriver.Firefox(executable_path=firefox_driver, options=options)

    def __getattr__(self, attr):
        if WebDriver._DRIVER and hasattr(WebDriver._DRIVER, attr):
            return getattr(WebDriver._DRIVER, attr)
        
        raise AttributeError(attr)
    
    @property
    def _driver(self):
        return WebDriver._DRIVER

    @_driver.setter
    def _driver(self, driver):
        WebDriver._DRIVER = driver

    @staticmethod
    def cleanup():
        ''' end an active web-driver, method is static so that we can
            call quit without accidently creating a web-driver beforehand '''
        if WebDriver._DRIVER:
            WebDriver._DRIVER.quit()

    def get(self, url, timeout=3, throw_error=False):
        ''' wrapper around the drivers get method, driver method can 
            still be accessed via web_driver._driver.get() '''
        try:
            self._driver.set_page_load_timeout(timeout)
            self._driver.get(url)
        except Exception as e:
            # the majority of providers should load in within the specified timeout, however there
            # are a few providers that will have loaded all the content we need but still attempt
            # to load bg items that we don't care about, for them we don't want to raise an error
            # as we can still use the webpage
            if throw_error:
                raise e