import time

from multiprocessing import Manager
from queue import Queue

from sources_api.extractors.source import extract_provider
from sources_api.extractors.source.factory import SourceExtractorFactory


FF_DRIVER = "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"


class SourceExtractorExecutor:
    ''' an adapter that allows the app to extract every direct and in-direct sources from a list of urls '''

    def __init__(self):
        self._sources = None
        self._iframes = None
        self._extractors = None

    def execute(self, urls):
        self._sources = []
        self._iframes = Queue()

        for i in urls:
            self._iframes.put(i)

        while not self._iframes.empty() != 0:
            url = self._iframes.get()
            provider = extract_provider(url)
            class_ = SourceExtractorFactory.get(provider)

            if class_:
                class_(url, self._sources, self._iframes, FF_DRIVER).run()

        return list(set(self._sources))

if __name__ == '__main__':
    urls = ["https://vidnext.net/streaming.php?id=ODgzMA==&title=John+Wick+HD-720p+&typesub=SUB&sub=L2pvaG4td2ljay1oZC03MjBwL2pvaG4td2ljay1oZC03MjBwLnZ0dA==&cover=L2pvaG4td2ljay1kc2cvY292ZXIucG5n"]
    urls += ["https://streamtape.com/e/eaP22qqXxYtwxp/the-lady-and-the-dale-season-1-episode-3.mp4"]


    e = SourceExtractorExecutor()
    sources = e.execute(urls)

    print("test", sources)