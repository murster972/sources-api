import re

from sources_api.extractors.source.extractors.jwplayer import JWPlayerExtractor

class MovCloudExtractor(JWPlayerExtractor):
    ''' extract meida-source from movcloud.net - small variaton on jw-player
        we can get a blob from the jw-player which we then need to do a GET
        to extract the m3u8 file '''

    def _extract_direct_sources(self):        
        # the jw-player file will be a blob source
        blob = list(super()._extract_direct_sources())[0]

        # if we load the blob-source it will return an HTML page
        # with the m3u8 sources within the body
        self._driver.get(blob)

        # extract and return the m3u8 sources
        return re.findall("https:\/\/.*?.m3u8", self._driver.page_source)

if __name__ == '__main__':
    url1 = "https://movcloud.net/embed/ss-MCQE4cTgg"

    from multiprocessing import Manager

    manager = Manager()
    output = manager.list()
    iframes = manager.Queue()

    p = [MovCloudExtractor(url1, output, iframes, "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox")]

    for i in p:
        i.join()

    print(output)
