import time 

from abc import ABC, abstractmethod

from sources_api.extractors.source import extract_provider
from sources_api.extractors import abs_address, enforce_protocol, WebDriver


class BaseExtractor(ABC):
    ''' extract media-sources from the video-player within the given url '''

    def __init__(self, url, sources, iframes, firefox_driver):
        self._firefox_driver = firefox_driver

        self._url = url
        self._provider = extract_provider(self._url)
        self._sources = sources
        self._iframes = iframes

        self._driver = WebDriver(firefox_driver)

    def run(self):
        try:
            self._driver.get(self._url)

            self._add_direct_sources()
            self._add_indirect_sources()
        except Exception as e:
            pass
            # logging.exception("error running extractor")
        finally:
            pass

    def _add_direct_sources(self):
        ''' add direct sources into the sources list '''
        try:
            for url in self._extract_direct_sources():
                url = abs_address(url, self._provider)
                self._sources.append(url)
        except Exception:
            pass
            # logging.exception("eror while adding direct sources into list...")
    
    def _add_indirect_sources(self):
        ''' add indirect sources into the iframe queue '''
        try:
            for iframe in self._extract_indirect_sources():
                iframe = enforce_protocol(iframe)
                self._iframes.put(iframe)
        except Exception:
            # logging.exception("eror while adding indirect sources into queue...")
            pass
    
    def _extract_direct_sources(self):   
        ''' stub method, used to extract any direct-sources '''
        return []    

    def _extract_indirect_sources(self):
        ''' stub method, used to extract any in-direct sources, i.e. iframes '''
        return []
        

if __name__ == '__main__':
    m = Manager()
    l = m.list()

    l += [1, 2, 3]

    print(l)

    # procs = [BaseExtractor(f"https://google.com", l, "/home/muzza-972/gitlab/heimdall/sources-api/sources_api/web-drivers/firefox") for i in range(5)]


    # # NOTE: time running as procs ~11s, time without ~50s
    # st = time.time()

    # for i in procs:
    #     i.join()

    # print(time.time() - st)