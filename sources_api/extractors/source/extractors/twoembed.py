from sources_api.extractors.source.extractors import BaseExtractor


class TwoEmbedExtractor(BaseExtractor):
    ''' extract media sources from within 2embed.ru, all sources are within
        i-frames so we'll be calling out to other extractors '''

    def _extract_indirect_sources(self):
        ''' extract all i-frame sources '''
        return self._driver.execute_script("""
            var item_elems = document.querySelectorAll(".item-server");
            var iframes = [];

            for(i = 0; i < item_elems.length; i++){
                let data_id = item_elems[i].getAttribute("data-id");

                $.ajax({
                    url: "/ajax/embed/play?id=" + data_id + "&_token=",
                    type: 'GET',
                    async: false, // NOTE: this needs to be set otherwise we'll exit before we get the results
                    success: function(e){ iframes.push(e.link) }
                })
            }

            return iframes """)

if __name__ == '__main__':
    url1 = "https://www.2embed.ru/embed/tmdb/movie?id=245891"

    from multiprocessing import Manager

    manager = Manager()
    output = manager.list()
    iframes = manager.Queue()

    p = [TwoEmbedExtractor(url1, output, iframes, "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox")]

    for i in p:
        i.join()

    while not iframes.empty():
        print(iframes.get())