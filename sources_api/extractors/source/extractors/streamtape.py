import time

from sources_api.extractors.source.extractors import BaseExtractor


class StreamTapeExtractor(BaseExtractor):
    ''' extract media-source from streamtape.com '''

    def _extract_direct_sources(self):
        # click the overlay to trigger the source being loaded
        elm = self._driver.find_element_by_css_selector(".plyr-overlay")
        elm.click()

        # give a split second for the players source to be updated
        time.sleep(1)

        yield self._driver.execute_script('return player.source')


if __name__ == '__main__':
    url1 = "https://streamtape.com/e/eaP22qqXxYtwxp/the-lady-and-the-dale-season-1-episode-3.mp4"

    from multiprocessing import Manager

    manager = Manager()
    output = manager.list()
    f = manager.Queue()

    p = [StreamTapeExtractor(url1, output, f, "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox")]

    for i in p:
        i.join()

    print(output)