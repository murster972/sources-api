from sources_api.extractors.source.extractors import BaseExtractor


class JWPlayerExtractor(BaseExtractor):
    ''' every provider that we encounter that uses a jw-player we can
        extract the source in the same way '''
        
    def _extract_direct_sources(self):
        yield self._driver.execute_script('return jwplayer().getPlaylist()[0].file')


if __name__ == '__main__':
    url1 = "https://streamsb.net/embed-xuia3d1bs3j5.html"
    # url2 = "https://streamsb.net/embed-8n0u911tk7cp.html"

    from multiprocessing import Manager

    manager = Manager()
    sources = manager.list()
    iframes = manager.Queue()

    p = [JWPlayerExtractor(url1, sources, iframes, "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"),]
        #  JWPlayerExtractor(url2, output, "/home/muzza-972/gitlab/heimdall/sources-api/sources_api/web-drivers/firefox")]

    for i in p:
        i.join()

    print(sources)