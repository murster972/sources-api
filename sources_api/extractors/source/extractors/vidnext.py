from sources_api.extractors.source.extractors import BaseExtractor
from sources_api.extractors.source import extract_provider


class VidNextExtractor(BaseExtractor):
    ''' extract media sources from vidcloud and vidnext '''
    
    def _extract_direct_sources(self):
        ''' extract the main-server source '''
        yield self._driver.execute_script('return jwplayer("myVideo").getPlaylist()[0].file')

    def _extract_indirect_sources(self):
        ''' extract all other sources '''
        return self._driver.execute_script("""
            var elements = document.querySelectorAll(".linkserver");
            var sources = new Array();

            for(i = 0; i < elements.length; i++){
                let val = elements[i].getAttribute("data-video");

                if(val != ""){
                    sources.push(val);
                }
            }

            return sources """)

if __name__ == '__main__':
    url1 = "https://vidnext.net/streaming.php?id=MzM2ODAw&title=Lovers%27+Lane+Murders+-+Season+1+Episode+1+-+No+One+Can+Hear+You+Scream&typesub=SUB&sub=&cover=Y292ZXIvbG92ZXJzLWxhbmUtbXVyZGVycy1zZWFzb24tMS5wbmc="

    from multiprocessing import Manager

    manager = Manager()
    output = manager.list()
    iframes = manager.Queue()

    p = [VidNextExtractor(url1, output, iframes, "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox")]

    for i in p:
        i.join()

    print("\nsources:", output)

    print("\niframes:")

    while not iframes.empty():
        print(iframes.get())