import re

def extract_provider(url):
    ''' extracts a provider base-url from a source link '''
    regex = r"https:\/\/[a-z0-9]*?\.[a-z]*"

    match = re.match(regex, url)

    return match.group() if match else None