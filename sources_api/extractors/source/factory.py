from sources_api.extractors.source.extractors.vidnext import VidNextExtractor
from sources_api.extractors.source.extractors.twoembed import TwoEmbedExtractor
from sources_api.extractors.source.extractors.jwplayer import JWPlayerExtractor
from sources_api.extractors.source.extractors.streamtape import StreamTapeExtractor
from sources_api.extractors.source.extractors.movcloud import MovCloudExtractor

from sources_api.providers import Providers

class SourceExtractorFactory:
    EXTRACTORS = {
            Providers.VIDNEXT: VidNextExtractor,
            Providers.VIDCLOUD9: VidNextExtractor,
            Providers.TWOEMBED: TwoEmbedExtractor,
            Providers.VIDCLOUD9: JWPlayerExtractor,
            Providers.UPSTREAM: JWPlayerExtractor,  
            Providers.STREAMSB: JWPlayerExtractor,
            Providers.STREAMTAPE: StreamTapeExtractor,
            Providers.MOVCLOUD: MovCloudExtractor,
        }

    @staticmethod
    def get(provider_url):
        ''' return the extractor-class for the given provider '''
        return SourceExtractorFactory.EXTRACTORS.get(provider_url, None)
