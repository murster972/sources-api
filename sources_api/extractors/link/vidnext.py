import re
import requests

from bs4 import BeautifulSoup
from abc import ABC

from sources_api.extractors.link import BaseLinkExtractor
from sources_api.extractors import abs_address

from sources_api.providers import Providers


class VidNextLinkExtractor(BaseLinkExtractor, ABC):
    @property
    def provider(self):
        return Providers.VIDNEXT


class VidNextMovieLinkExtractor(VidNextLinkExtractor):
    ''' extract link to a movie items page, since this provider does not
        consitently provide the release-year will be a bit of guess work
        if we get returned multiple hrefs for a single target-st
        
        Since names don't follow a standard pattern movies will be hit or
        miss for vidnext, e.g. some have the full imdb title while others
        have shortned names so won't match the full imdb name. '''
    def extract(self):
        # get all provider hrefs
        hrefs = [i for i in self._get_hrefs() if not i.startswith("/search/")]

        if len(hrefs) == 1:
            return hrefs[0]
        
        # check for hrefs that contain the release-year
        contains_year = [i for i in hrefs if str(self.release_year) in i]

        # if there are hrefs that contain the year, we only want to search
        # them now
        if contains_year:
            hrefs = contains_year

        # find the shortest from the multiple matches
        shortest = self._get_shortest(hrefs)

        return abs_address(shortest, self.provider)

    def _get_shortest(self, hrefs):
        shortest = ""

        for href in hrefs:
            if not shortest or len(href) < len(shortest):
                shortest = href

        return shortest
    
    def _get_hrefs(self):
        ''' get all hrefs on the page that match our item '''
        anything = "[a-zA-Z0-9+\/\-_:.]*?"
        name = self.name.strip().replace(" ", anything)

        # NOTE: (?i) makes the match case insensitive
        regex = f'(?i)href="{anything}{name}{anything}"'

        # remove the href at the start and the quote at the end
        return [i[6:-1] for i in re.findall(regex, self.html)]
    

class VidNextTVLinkExtractor(VidNextLinkExtractor):
    ''' extarct links to specific episodes for a given tv-show

        since search is horrible, we don't use the html rather
        we know the format of the url so we just check if the link
        exists '''
    def extract(self):
        methods = [
            self._get_link_padd,
            self._get_link_padd_episode,
            self._get_link_padd_season,
            self._get_link_no_padd
        ]

        for creator in methods:
            url = creator()

            if self._check_link(url):
                return url
        
        return None

    def _check_link(self, url):
        ''' check the link exists '''
        # include user-agent to help prevent 403's
        user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"

        try:
            req = requests.get(url, timeout=2, headers={"user-agent": user_agent})

            if req.text.strip() == "404":
                raise Exception()

            return True
        except Exception:
            return False

    def _get_link(self, season, episode):
        ''' generate format provider/videos/name-season-episode-episode_name '''
        name = self.name.replace(" ", "-")
        episode_name = self.episode_name.replace(" ", "-")

        return f"{self.provider}/videos/{name}-season-{season}-episode-{episode}-{episode_name}"

    def _get_link_padd(self):
        return self._get_link(self.season.zfill(2), self.episode.zfill(2))
    
    def _get_link_padd_season(self):
        return self._get_link(self.season.zfill(2), self.episode)

    def _get_link_padd_episode(self):
        return self._get_link(self.season, self.episode.zfill(2))

    def _get_link_no_padd(self):
        return self._get_link(self.season, self.episode)


if __name__ == '__main__':
    html = """

        <!DOCTYPE html>
        <html lang="en-US"
            xmlns="http://www.w3.org/1999/xhtml"
            itemscope itemtype="http://schema.org/WebPage">
        <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
            <meta charset="UTF-8"/>
        <meta name="viewport"
            content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>

        <link rel="profile" href="https://gmpg.org/xfn/11">

        <link rel="shortcut icon" href="/favicon.png">

        <title>Vidcloud - Watch movie and tvshow anywhere </title>

        <meta name="robots" content="noodp, noydir" />
        <meta name="description" content="Vidcloud - Watch movie and tvshow anywhere ">
        <meta name="keywords" content="Vidcloud - Watch movie and tvshow anywhere ">
        <meta itemprop="image" content="/images/logo.png"/>

        <meta property="og:locale" content="en_US"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="Vidcloud - Watch movie and tvshow anywhere "/>
        <meta property="og:description" content="Vidcloud - Watch movie and tvshow anywhere ">
        <meta property="og:url" content=""/>
        <meta property="og:image" content="/images/logo.png"/>
        <meta property="og:image:secure_url" content="/images/logo.png"/>

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Vidcloud - Watch movie and tvshow anywhere "/>
        <meta property="twitter:description" content="Vidcloud - Watch movie and tvshow anywhere "/>

        <link rel="canonical" href="/search.html"/>
        <link rel="alternate" hreflang="en-us" href="/search.html"/>



            
                <link rel="stylesheet" type="text/css" href="https://vidnext.net/video/css/font-awesome.min.css??v=7.5" />
                <link rel="stylesheet" type="text/css" href="https://vidnext.net/video/css/style.css??v=7.5" />
            
            <script type="text/javascript" src="https://vidnext.net/video/js/jquery.js"></script>
        </head>
        <body>
        <script>
                var base_url =  '//' + document.domain + '/';
        </script>
        <div id="wrapper_bg" class="">
                <section id="wrapper" class="wrapper">
                    <div id="main_bg" class="main">
                        <div class="main-content">
            <header class="header vc_row wpb_row vc_row-fluid">
            <div class="top-header vc_col-sm-12">
                <div class="logo vc_col-sm-2">
                    <a href="/">
                    <img src="https://vidnext.net/img/logo_vid.png?1" class="retina-header" alt="Vidcloud - Watch movie and tvshow anywhere " />            </a>
                </div>
                <div class="header_search">
                    <div class="search sb-search" id="sb-search">
                    <div class="form">
        <form onsubmit="" id="search-form" action="" method="get" class=" gray-form lap">
            <div class="row">
                <input placeholder="Search here..." class="footer_search_input" name="keyword_search" id="keyword" type="text" value="" autocomplete="off" onclick="" type="text">            
                <input class="btngui btn btn-primary sb-search-submit" type="submit" onclick="do_search();">
                <input id="key_pres" name="key_pres" value="" type="hidden" />
                <input id="link_alias" name="link_alias" value="" type="hidden" />
                <input id="keyword_search_replace" name="keyword_search_replace" value="" type="hidden" />
                <span class="sb-icon-search icon-search blue-button" onclick="do_search();">
                        <i class="fa fa-search" aria-hidden="true"></i>
                </span>
            </div>
            <div class="load search"></div>
            <div id="header_search_autocomplete"> 
                        
            </div>
        </form>
        </div><div class="clr"></div>
                    </div>
                </div>
                <div id="header-social">
                            <a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-google-plus"></i></a><a href="#"><i class="fa fa-instagram"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><a href="#"><i class="fa fa-tumblr"></i></a>					<a href="http://videotube.marstheme.com/feed/rss/"><i class="fa fa-rss"></i></a>
                        </div>
                <div class="clearfix"></div>
            </div>
        </header>
        </div>
        <div class="clearfix"></div>
        <div id="navigation-wrapper">
            <div class="main-content">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                </button>
                <ul id="menu-header-menu">
                    <li><a href="https://vidnext.net/">Home</a></li>
                    <li><a href="https://vidnext.net/movies">Movies</a></li>
                    <li><a href="https://vidnext.net/series">TV Series</a></li>
                    <li><a href="https://vidnext.net/cinema-movies">Cinema Movies</a></li>
                    <li><a href="https://vidnext.net/recommended-series">Featured Series</a></li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
                        <div class="main-content">
                            
        <div class="main-inner">
            <div class="section-header">
                <h3 class="widget-title"><i class="fa fa-play"></i> Result search</h3>
            </div>
            <div class="clearfix"></div>
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1404913114846">
                <div class="vc_col-sm-12 wpb_column column_container">
                    <div class="wpb_wrapper">
                            <div class="video_player followed  default">
                                <!-- for -->
                                <ul class="listing items">
                                    
                        <li class="video-block ">
                    <a href="/videos/king-kong-hd-720p">
                        
                        <div class="img">
                                <div class="picture">
                                    <img src="https://cdn.themovieseries.net//king-kong-swa/cover.png" alt="King Kong " />
                                </div>
                                <div class="hover_watch"><div class="watch"></div></div>
                        </div>
                        <div class="name">
                                King Kong             
                        </div>
                        <div class="meta">
                            <span class="date">2016-05-24 10:32:52</span>
                        </div>
                    </a>
                </li>
                        <li class="video-block ">
                    <a href="/videos/king-kong-2005">
                        
                        <div class="img">
                                <div class="picture">
                                    <img src="https://cdn.themovieseries.net//king-kong-2005-bnw/cover.png" alt="King Kong (2005)" />
                                </div>
                                <div class="hover_watch"><div class="watch"></div></div>
                        </div>
                        <div class="name">
                                King Kong (2005)            
                        </div>
                        <div class="meta">
                            <span class="date">2016-05-24 10:14:59</span>
                        </div>
                    </a>
                </li>
                        <li class="video-block ">
                    <a href="/videos/king-kong-vs-godzilla-sd">
                        
                        <div class="img">
                                <div class="picture">
                                    <img src="https://cdn.themovieseries.net//king-kong-vs-godzilla-ucc/cover.png" alt="King Kong vs. Godzilla SD" />
                                </div>
                                <div class="hover_watch"><div class="watch"></div></div>
                        </div>
                        <div class="name">
                                King Kong vs. Godzilla SD            
                        </div>
                        <div class="meta">
                            <span class="date">2016-05-24 09:43:51</span>
                        </div>
                    </a>
                </li>
                
                <div class="clr"></div>                        </ul>
                                <!-- end for -->          
                            </div>                     
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="margin-right: auto ; margin-left: auto"> 
                    <div class="contus_tablenav-pages">
                        <div class="pagination">
                            <nav>
                            
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="footer">
                <div class="main-content">
                    <!--
                    <div class="row">
                        <div id="meta-3" class="col-sm-3 widget widget_meta">
                            <h4 class="footer-widget-title">Meta</h4>			
                        </div>
                    <div id="mars-keycloud-widgets-2" class="col-sm-3 widget mars-keycloud-widgets">
                        <h4 class="footer-widget-title">Tags Cloud</h4>
                        </div>
                        <div id="mars-connected-widget-2" class="col-sm-3 widget mars-connected-widget">
                            <h4 class="footer-widget-title">Stay Connected</h4>
                            <ul class="list-unstyled social">
                                <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i> Google Plus</a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i> Instagram</a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i> Linkedin</a></li>
                                <li><a href="#"><i class="fa fa-tumblr"></i> Tumblr</a></li>
                                <li><a href="#"><i class="fa fa-rss"></i> RSS</a></li>
                            </ul>
                        </div>
                        <div id="text-7" class="col-sm-3 widget widget_text">
                            <h4 class="footer-widget-title">ABOUT VIDEOTUBE</h4>			
                            <div class="textwidget">

                            </div>
                        </div>			
                    </div>
                    -->
                    <div class="clearfix"></div>
                    <div class="copyright">
                        <p>Copyright Movies All rights reserved.</p>
                    </div>
                </div>
        </div>
                </section>
        </div>
        <div id="off_light"></div>
        <div class="clr"></div>
        <div class="mask"></div>
        <script type="text/javascript" src="https://vidnext.net/video/js/hamfunction.js?v=7.5"></script>
        <script type="text/javascript" src="https://vidnext.net/video/js/combo.js?v=7.5"></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-101166009-15"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-101166009-15');
        </script>
        </body>
        </html>
    """

    a = VidNextTVHrefExtractor(html, {"name": "the middle", "release-year": 0, "season": 1, "episode": 200, "episode-name": "pilot"})
    href = a.extract()

    print(href)