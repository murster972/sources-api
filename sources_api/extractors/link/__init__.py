from abc import abstractmethod, ABC



class BaseLinkExtractor(ABC):
    ''' extract the link to an items page
    
        :param html: source-code of the search page
        :param target_str: item strnig to search for, will vary from
                           just the movie/show name to a combo of
                           the season and episode aswell 
        :param release_year: year the item was released '''
    def __init__(self, html, item_info):
        self.html = html

        self.name = item_info.get("name", "")
        self.release_year = str(item_info.get("release-year", ""))
        self.season = str(item_info.get("season", ""))
        self.season_release_year = str(item_info.get("season-release-year", ""))
        self.episode = str(item_info.get("episode", ""))
        self.episode_name = item_info.get("episode-name", "")

    @property
    @abstractmethod
    def provider(self):
        pass

    @abstractmethod
    def extract(self):
        pass