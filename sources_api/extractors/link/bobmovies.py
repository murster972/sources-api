from bs4 import BeautifulSoup

from abc import ABC, abstractmethod

from sources_api.extractors.link import BaseLinkExtractor
from sources_api.extractors import abs_address

from sources_api.providers import Providers


class BobMoviesLinkExtractor(BaseLinkExtractor, ABC):
    @property
    def provider(self):
        return Providers.BOBMOVIES

    def _get_items(self):
        ''' extract all items that match our targets name '''
        soup = BeautifulSoup(self.html, "html.parser")

        items = []

        for element in soup.find_all(class_="flw-item"):
            info = element.find(class_="film-detail")

            a_tag = info.find("h3").find("a")

            name = a_tag["title"]
            href = abs_address(a_tag["href"], self.provider)
            year = info.find_all("span")[1].getText()

            if name.lower() == self.name.lower():
                items.append((name, href, year))

        return items


class BobMoviesMovieLinkExtractor(BobMoviesLinkExtractor):
    ''' extract a direct link to a movies page '''
    def extract(self):
        for name, href, release in self._get_items():
            if release == self.release_year:
                return href


class BobMoviesTVLinkExtractor(BobMoviesLinkExtractor):
    ''' extract a link to a tv-shows overview page, the provider
        uses on page per show that contains links to all seasons
        and episodes '''
    def extract(self):
        # tv-shows don't contain a year on the search page so
        # we just return the first item we find
        for name, href, release in self._get_items():
            return href
    

if __name__ == '__main__':
    import requests

    ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"
    html = requests.get("https://bobmovies.cc/search/king-kong", headers={"user-agent": ua}).text

    a = BobMoviesMovieLinkExtractor(html, {"name": "king kong", "release-year": 2005})

    url = a.extract()

    print(url)

    # ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"
    # html = requests.get("https://bobmovies.cc/search/the-middle", headers={"user-agent": ua}).text

    # a = BobMoviesTVLinkExtractor(html, {"name": "the middle", "release-year": 2009})

    # url = a.extract()

    # print(url)