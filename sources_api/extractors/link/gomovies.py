from bs4 import BeautifulSoup

from abc import ABC, abstractmethod

from sources_api.extractors.link import BaseLinkExtractor
from sources_api.extractors import abs_address

from sources_api.providers import Providers



class GoMoviesLinkExtractor(BaseLinkExtractor, ABC):
    ''' extract a link for an item from the gomovies
        search page based on the items name and year '''
    @property
    def provider(self):
        return Providers.GOMOVIES

    @property
    @abstractmethod
    def target_release_year(self):
        ''' returns the release-year we're searching against '''
        pass
    
    @property
    @abstractmethod
    def target(self):
        ''' the name of the item to search for on the search page '''
        pass

    def extract(self):
        for name, href, release in self._get_items():
            if release == str(self.target_release_year):
                return href

    def _get_items(self):
        ''' extract all items that match our targets name '''
        soup = BeautifulSoup(self.html, "html.parser")

        items = []

        for element in soup.find_all(class_="poster_poster__2Tnqo"):
            a_tag = element.find("a")

            href = abs_address(a_tag["href"], self.provider)
            name = a_tag.find("p").getText().strip()
            release = a_tag.find(class_="poster_release__31qZF").getText().strip()

            if name.lower() == self.target.lower():
                items.append((name, href, release))

        return items


class GoMoviesMovieLinkExtractor(GoMoviesLinkExtractor):
    ''' extract a direct link to a movies page '''
    @property
    def target(self):
        return self.name

    @property
    def target_release_year(self):
        return self.release_year


class GoMoviesTvLinkExtractor(GoMoviesLinkExtractor):
    ''' extract a link to a tv-season page, this provider groups
        all episodes into a single page '''
    @property
    def target(self):
        return f"{self.name} - season {self.season}"

    @property
    def target_release_year(self):
        ''' gomovies groups episodes into per-season pages so we're targeting the
            year the season was released '''
        return self.season_release_year


if __name__ == '__main__':
    import requests

    # ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"
    # html = requests.get("https://gomovies.beer/search/king%20kong", headers={"user-agent": ua}).text

    # a = GoMoviesMovieLinkExtractor(html, {"name": "king kong", "release-year": 2005})
    # href = a.extract()

    # print(href)

    ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"
    html = requests.get("https://gomovies.beer/search/the%20middle%20season%203", headers={"user-agent": ua}).text

    a = GoMoviesTvLinkExtractor(html, {"name": "the middle", "release-year": 2009, "season-release-year": 2011, "season": 3, "episode": 1, "episode-name": ""})
    href = a.extract()

    print(href)