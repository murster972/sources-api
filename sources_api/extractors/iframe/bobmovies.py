from bs4 import BeautifulSoup


from sources_api.extractors.iframe import OnloadIFrameExtractor


class BobMoviesTVIFrameExtractor(OnloadIFrameExtractor):
    ''' Navigates through an items load page to load ane extract the iframe source
        for a given episode on bobmovies - bobmovies groups all seasons (and their episodes)
        into a single page '''
    def _get_html(self):
        super()._get_html()

        # get the link to the specific episode
        soup = BeautifulSoup(self._driver.page_source, 'html.parser')
        anchor = soup.find(attrs={"data-number": self.item_info["episode"], "data-s-number": self.item_info["season"]})
                
        # we can find the episode item via css-selectors
        css_selector = f'a.episode-item[data-number="{self.item_info["episode"]}"][data-s-number="{self.item_info["season"]}"]'

        # NOTE: if this is not done selenium will through out an error stating it was
        #       unable to scroll to the item
        #
        # unhide the episodes parent so that we can click on the episode item
        self._driver.execute_script(f"""
                    let episode = $('{css_selector}')
                    episode.parent().parent().parent().addClass("active show") """)

        # ...and finally, we click on the episode element to load it into the iframe
        element = self._driver.find_element_by_css_selector(css_selector)
        element.click()

        return self._driver.page_source

if __name__ == '__main__':
    driver = "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"
    url = "https://bobmovies.cc/tv/watch-the-good-doctor-online-39526"
    a = BobMoviesTVIFrameExtractor(url, {"episode": 1, "season": 2}, driver)

    print(a.extract())