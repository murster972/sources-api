from bs4 import BeautifulSoup
from abc import ABC, abstractmethod

from sources_api.extractors import enforce_protocol, WebDriver, get_html



class BaseIFrameExtractor(ABC):
    ''' extract iframe sources from the given item-page '''
    def __init__(self, url, item_info, ff_driver=""):
        self.url = url
        self.item_info = item_info
        self.ff_driver = ff_driver

        self._driver = WebDriver(ff_driver)

    def extract(self):
        try:
            html = self._get_html()

            return self._extract_src(html)
        except Exception as e:
            pass

    @abstractmethod
    def _get_html(self):
        ''' return the html from the given link, ensuring that the iframe
            and its source has been loaded in '''
        pass

    def _extract_src(self, html):
        ''' extract all iframe links from the given html '''
        # NOTE: this is assuming that theres only a single iframe
        #       link on the page, which tracks for all the providers
        #       we've seen so far, but something to consider if we
        #       run into issues in the future.
        soup = BeautifulSoup(html, 'html.parser')
        src = soup.find("iframe")["src"]
        
        # since iframes are external links we don't have to worry about them
        # being relative, however we do want to ensure we enforce a protocol
        # to avoid any addresses starting with "//"
        return enforce_protocol(src)


class OnloadIFrameExtractor(BaseIFrameExtractor):
    ''' This extractor assumes that the iframe source is loaded via
        some Js that's trigger on-load, so we need to use selenium to get it.
        
        Loading the driver is an expensive operation and we only want todo it when
        needed, hence why we other extractors like DirectIFrameExtractor '''
    def _get_html(self):
        self._driver.get(self.url)

        return self._driver.page_source


class DirectIFrameExtractor(BaseIFrameExtractor):
    ''' This extractor assumes that we can extract the iframes source as soon
        as we load the page all we have todo is regex match for an iframe,
        i.e. we don't have to click any elements to load the iframe source. '''
    def _get_html(self):
        return get_html(self.url)