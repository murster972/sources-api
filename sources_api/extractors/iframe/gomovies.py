from sources_api.extractors.iframe import DirectIFrameExtractor


#TODO;
#   - gomovies has multiple sources listed on the site
#   - and hence multiple iframe sources
#   - return them all instead of just one

class GoMoviesBaseIFrameExtractor(DirectIFrameExtractor):
    def _get_html(self):
        # we specify not to throw any errors on load due to the fact that gomovies
        # will continue to try and load things in the bg after we've loaded in the
        # html and we'll timeout before it has finished loading fully.
        self._driver.get(self.url, timeout=3, throw_error=False)


class GoMoviesMovieIFrameExtractor(GoMoviesBaseIFrameExtractor):
    def _get_html(self):
        super()._get_html()

        # the actual movie page is located in an href on the play button
        a_tag = self._driver.find_element_by_css_selector(".watch_player__1NZNP")
        href = a_tag.get_attribute("href")

        try:
            self._driver.get(href)
        except Exception:
            # ignore any timeout errors
            pass

        return self._driver.page_source


class GoMoviesTVIFrameExtractor(GoMoviesBaseIFrameExtractor):
    ''' Navigates through an items load page to load ane extract the iframe source
        for a given episode on gomovies - gomovies groups seasons episodes into a
        single page '''
    def _get_html(self):
        super()._get_html()

        # we can use css selector to get our episodes element
        #
        # NOTE: we do a partial match instead of a full match since
        #       the titles are not always the same, some contain episode
        #       names some don't, but they always seem to have the same
        #       format for the season and episode number, e.g.
        #       Season N Episode N (note the capitalization)
        css_selector = f'a[title*="Season {self.item_info["season"]} Episode {self.item_info["episode"]}"'

        self._driver.find_element_by_css_selector(css_selector).click()

        return self._driver.page_source

if __name__ == '__main__':
    driver = "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"
    url = "https://gomovies.beer/watching/DJMJDujj/john-wick-2014"
    a = GoMoviesMovieIFrameExtractor(url, {"episode": 2, "season": 15}, driver)

    print(a.extract())