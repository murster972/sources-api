class Routes:
    ''' Enum of all api routes '''
    MOVIE   = "/sources/movie"
    TVSHOW  = "/sources/tvshow"