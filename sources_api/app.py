import falcon
import signal

from sources_api.routes import Routes
from sources_api.api_handler.handler import APIHandler
from sources_api.api_handler.data_validator import MovieDataValidator, TvShowDataValidator
from sources_api.extractors import WebDriver

movie_valid = MovieDataValidator()
tv_valid = TvShowDataValidator()

app = falcon.API()
app.add_route(Routes.MOVIE, APIHandler(movie_valid))
app.add_route(Routes.TVSHOW, APIHandler(tv_valid))

clenaup = lambda signum, frame: WebDriver.cleanup()

signal.signal(signal.SIGINT, clenaup)
signal.signal(signal.SIGTERM, clenaup)