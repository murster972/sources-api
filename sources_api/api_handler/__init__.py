class InvalidRequest(Exception):
    pass


class ErrorCodes:
    NO_ERROR            = 0
    UNKNOWN_ERROR       = 1
    INVALID_REQUEST     = 2


class ResponseData(dict):
    ''' represents the data we'll send in the response, 
        acts like an attribute dict '''

    def __init__(self):
        super().__init__()
        self.update({"sources": [], "error": ErrorCodes.NO_ERROR})

    def __getattr__(self, key):
        if key in self:
            return self[key]
        else:
            raise AttributeError(f"unknown attribute '{key}'")

    def __setattr__(self, key, value):
        self[key] = value