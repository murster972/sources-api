import json
import logging
import falcon

from enum import Enum

from sources_api.api_handler import InvalidRequest, ErrorCodes, ResponseData

from sources_api.extractors.source.executor import SourceExtractorExecutor
from sources_api.searcher.executor import SearchExecutor


FF_DRIVER = "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"


class APIHandler:
    ''' responsible for dealing with api requests '''

    def __init__(self, data_validator):
        self._data_validator = data_validator

        self._searcher = SearchExecutor(FF_DRIVER)
        self._source_extractor = SourceExtractorExecutor()

    def on_post(self, req, resp):
        ''' build up a response for the users request '''
        try:
            # create rsponse data now to ensure that we always respond
            # with the same data struct
            resp_data = ResponseData()

            # check data is valid - since the content-type is json falcon
            # automatically converts into a dict for us
            data = req.media

            print(data)

            if not self._data_validator.validate(data):
                raise InvalidRequest()

            # search for iframes
            iframes = self._searcher.execute(data, req.path)

            # source extraction
            resp_data.sources = list(self._source_extractor.execute(iframes))
        except (TypeError, InvalidRequest, AttributeError) as e:
            resp_data.error = ErrorCodes.INVALID_REQUEST
            resp.status = falcon.HTTP_400
        except:            
            resp_data.error = ErrorCodes.UNKNOWN_ERROR
            resp.status = falcon.HTTP_500

            logging.exception("Unexpected error while handling api-request")
        finally:
            # ensures that we always respond with the same data struct
            # even when an error has occured
            resp.body = json.dumps(resp_data)