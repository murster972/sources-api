from abc import ABC


class DataValidator(ABC):
    ''' check if passed dict matches the defined schema '''
    def __init__(self):
        self.schema = None

    def validate(self, data):
        ''' checks if the request data matches the schema '''
        for item, value in data.items():
            print(item, value)
            if item not in self.schema or not isinstance(value, self.schema[item]):
                return False

        return True
    

class MovieDataValidator(DataValidator):
    def __init__(self):
        self.schema = {"name": str, "release-year": int}


class TvShowDataValidator(DataValidator):
    def __init__(self):
        self.schema = {"name": str, "release-year": int, "season": int, "season-release-year": int,
                       "episode": int, "episode-name": str}