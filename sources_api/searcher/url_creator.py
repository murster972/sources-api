from abc import ABC, abstractmethod
from urllib import parse


class URLCreator(ABC):
    ''' generates a search-url for the given base_url '''

    @staticmethod
    @abstractmethod
    def create(base_url, item):
        pass

class URLCreatorMethodOne(URLCreator):
    @staticmethod
    def create(base_url, item):
        return f"{base_url}/search/{item}"

class URLCreatorMethodTwo(URLCreator):
    @staticmethod
    def create(base_url, item):
        return f"{base_url}/search/{item.replace(' ', '-')}"

class URLCreatorMethodThree(URLCreator):
    @staticmethod
    def create(base_url, item):
        return f"{base_url}/search/{item.replace(' ', '+')}"

class URLCreatorMethodFour(URLCreator):
    @staticmethod
    def create(base_url, item):
        keyword = parse.urlencode({"keyword": item})
        return f"{base_url}/search.html?{keyword}"