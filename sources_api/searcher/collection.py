from abc import ABC

from sources_api.providers import Providers


# NOTE: don't like having to import all these manually, doesn't scale
#       very well if we want to keep adding in more providers/searchers
from sources_api.searcher.searchers.bobmovies import BobMoviesMovieSearcher
from sources_api.searcher.searchers.bobmovies import BobMoviesTVSearcher
from sources_api.searcher.searchers.gomovies import GoMoviesMovieSearcher
from sources_api.searcher.searchers.gomovies import GoMoviesTVSearcher
from sources_api.searcher.searchers.vidnext import VidNextMovieSearcher
from sources_api.searcher.searchers.vidnext import VidNextTVSearcher


class _Collection(ABC):
    ''' represents a collection of searchers '''
    
    SEARCHERS = {}

    def get(self):
        return self.SEARCHERS.items()


class MovieSearchers(_Collection):
    SEARCHERS = {
        Providers.BOBMOVIES: BobMoviesMovieSearcher,
        Providers.GOMOVIES: GoMoviesMovieSearcher,
        Providers.VIDNEXT: VidNextMovieSearcher,
        Providers.VIDCLOUD9: VidNextMovieSearcher
    }


class TvShowSearchers(_Collection):
    SEARCHERS = {
        Providers.BOBMOVIES: BobMoviesTVSearcher,
        Providers.GOMOVIES: GoMoviesTVSearcher,
        Providers.VIDNEXT: VidNextTVSearcher,
        Providers.VIDCLOUD9: VidNextTVSearcher
    }