from sources_api.routes import Routes
from sources_api.searcher.collection import MovieSearchers, TvShowSearchers


# TODO: worth checking the following
#   1. if this is faster using multiprocessing to run all searchers at once
#   2. or if its faster using a singleton webdriver and leaving it single process


class SearchExecutor:
    ''' Used to execute a search across all searches in a collection.
        Collections are based on the api-route. '''

    def __init__(self, firefox_driver):
        self.firefox_driver = firefox_driver

        self._collections = {
            Routes.MOVIE: MovieSearchers(),
            Routes.TVSHOW: TvShowSearchers()
        }

    def execute(self, item_info, route):
        iframes = []

        # execute all searches
        for provider, searcher in self._collections[route].get():
            try:
                s = searcher(item_info, provider, self.firefox_driver)
                r = s.search()

                print(provider, r)

                if r:
                    iframes.append(r)
            except Exception as e:
                raise e
                # logging.exception("error searching")
                pass

        return list(set(iframes))

if __name__ =='__main__':
    driver = "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"
    item = {"name": "The Middle", "release-year": 2009, "season-release-year": 2013, "episode": 3, "season": 5, "episode-name": "The Potato"}
    # item = {"name": "the fast and the furious", "release-year": 2014}

    a = SearchExecuter(driver)

    s = a.execute(item, Routes.TVSHOW)

    print(s)