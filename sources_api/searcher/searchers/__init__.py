from abc import ABC, abstractmethod

from sources_api.extractors import get_html
from sources_api.extractors.iframe import OnloadIFrameExtractor


class BaseSearcher(ABC):
    ''' Search a provider for links to a media-player containning the given item.
        Acts as a facade around the url-creator and link/iframe extractors '''
    def __init__(self, item_info, provider, firefox_driver):
        self.item_info = item_info
        self.firefox_driver = firefox_driver
        self.provider = provider
    
    @abstractmethod
    def _create_url(self):
        pass

    @abstractmethod
    def _extract_link(self, html):
        pass

    def _extract_iframe(self, link):
        return OnloadIFrameExtractor(link, self.item_info, self.firefox_driver).extract()

    def search(self):
        try:
            url = self._create_url()            
            html = get_html(url)
            link = self._extract_link(html)            
            iframe = self._extract_iframe(link)

            return iframe
        except Exception as e:
            raise e
            pass