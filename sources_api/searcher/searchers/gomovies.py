from sources_api.extractors.iframe.gomovies import GoMoviesTVIFrameExtractor, GoMoviesMovieIFrameExtractor

from sources_api.extractors.link.gomovies import GoMoviesMovieLinkExtractor, GoMoviesTvLinkExtractor

from sources_api.searcher.searchers import BaseSearcher
from sources_api.searcher.url_creator import URLCreatorMethodOne

from sources_api.providers import Providers


class GoMoviesMovieSearcher(BaseSearcher):
    def _create_url(self):
        return URLCreatorMethodOne.create(self.provider, self.item_info["name"])

    def _extract_link(self, html):
        return GoMoviesMovieLinkExtractor(html, self.item_info).extract()

    def _extract_iframe(self, link):
        return GoMoviesMovieIFrameExtractor(link, self.item_info, self.firefox_driver).extract()


class GoMoviesTVSearcher(BaseSearcher):
    def _create_url(self):
        item = f'{self.item_info["name"]} season {self.item_info["season"]}'
        return URLCreatorMethodOne.create(self.provider, item)

    def _extract_link(self, html):
        return GoMoviesTvLinkExtractor(html, self.item_info).extract()
        
    def _extract_iframe(self, link):
        return GoMoviesTVIFrameExtractor(link, self.item_info, self.firefox_driver).extract()


if __name__ == '__main__':
    driver = "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"
    item = {"name": "The Middle", "release-year": 2013, "episode": 3, "season": 5, "episode-name": "hecking order"}
    # item = {"name": "john wick", "release-year": 2014}

    v = GoMoviesTVSearcher(item, driver)

    print(v.search())