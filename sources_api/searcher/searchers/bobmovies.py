from sources_api.extractors.iframe.bobmovies import BobMoviesTVIFrameExtractor

from sources_api.extractors.link.bobmovies import BobMoviesMovieLinkExtractor, BobMoviesTVLinkExtractor

from sources_api.searcher.searchers import BaseSearcher
from sources_api.searcher.url_creator import URLCreatorMethodTwo

from sources_api.providers import Providers


class BobMoviesBaseSearcher(BaseSearcher):
    def _create_url(self):
        return URLCreatorMethodTwo.create(self.provider, self.item_info["name"])


class BobMoviesMovieSearcher(BobMoviesBaseSearcher):
    def _extract_link(self, html):
        return BobMoviesMovieLinkExtractor(html, self.item_info).extract()


class BobMoviesTVSearcher(BobMoviesBaseSearcher):
    def _extract_link(self, html):
        return BobMoviesTVLinkExtractor(html, self.item_info).extract()
        
    def _extract_iframe(self, link):
        return BobMoviesTVIFrameExtractor(link, self.item_info, self.firefox_driver).extract()


if __name__ == '__main__':
    driver = "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"
    # item = {"name": "The Middle", "release-year": 2011, "episode": 3, "season": 5, "episode-name": "hecking order"}
    item = {"name": "john wick chapter 2", "release-year": 2014}

    v = BobMoviesTVSearcher(item, "https://bobmovies.cc", driver)

    print(v.search())