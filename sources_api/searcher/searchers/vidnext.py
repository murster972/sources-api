from sources_api.extractors.iframe import OnloadIFrameExtractor
from sources_api.extractors.link.vidnext import VidNextMovieLinkExtractor, VidNextTVLinkExtractor

from sources_api.searcher.searchers import BaseSearcher
from sources_api.searcher.url_creator import URLCreatorMethodFour

from sources_api.providers import Providers


class VidNextMovieSearcher(BaseSearcher):
    def _create_url(self):
        return URLCreatorMethodFour.create(self.provider, self.item_info["name"])

    def _extract_link(self, html):
        return VidNextMovieLinkExtractor(html, self.item_info).extract()


class VidNextTVSearcher(BaseSearcher):
    def _create_url(self):
        # NOTE: the link extractor for vidnext tv does not require html, hence we don't need
        #       to create a url
        return self.provider

    def _extract_link(self, html):
        return VidNextTVLinkExtractor(html, self.item_info).extract()
        

if __name__ == '__main__':
    driver = "/home/muzza-972/gitlab/heimdall/sources-api/web-drivers/firefox"
    # item = {"name": "The Middle", "release-year": 2011, "episode": 3, "season": 3, "episode-name": "hecking order"}
    item = {"name": "john wick", "release-year": 2014}
    v = VidNextMovieSearcher(item, "https://vidnext.net", driver)


    print(v.search())