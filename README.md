# Sources API
JSON API capable of extracting media-sources for a given movie/episode. Used internally within Heimdall

# Routes
## IP:PORT/sources/movie
### Schema
#### Request
| Key          | Data-Type |
|--------------|-----------|
| name         | str       |
| release-year | int       |
#### Response
| Key          | Data-Type |
|--------------|-----------|
| sources      | list[obj] |
| error        | int       |

### Example
```bash
curl -X POST 127.0.0.1:8000/sources/movie -H "Content-Type: application/json" -d '{"name": "John Wick", "release-year": 2014}'
```
```json
{
    "name": "John Wick",
    "release-year": 2014
}

{
    "sources": [{"link": "https://url.com/johnwick.mp4", "steaming-type": DIRECT}],
    "error": 0
}

```

## IP:PORT/sources/tvshow
### Schema
#### Request
| Key          | Data-Type |
|--------------|-----------|
| name         | str       |
| release-year | int       |
| season       | int       |
| season-release-year | int       |
| episode      | int       |
| episode-name | str       |

#### Response
| Key          | Data-Type |
|--------------|-----------|
| sources      | list[obj] |
| error        | int       |
### Example
```bash
curl -X POST 127.0.0.1:8000/sources/tvshow -H "Content-Type: application/json" -d '{"name": "The Middle", "release-year": 2009, "season-release-year": 2011, "season": 3, "episode": 1}'
```
```json
{
    "name": "The Middle",
    "release-year": 2009,
    "season": 1,
    "season-release-year": 2009,
    "episode": 1,
    "episode-name": "Pilot"
}

{
    "sources": [{"link": "https://url.com/themiddles1e1.h3u8", "steaming-type": HLS}],
    "error": 0
}

```